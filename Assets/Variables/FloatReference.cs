using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FloatReference
{
    public bool useConstant = true;
    public FloatVariable constantVariable;
    public FloatVariable variable;

    public float Value
    {
        get
        {
            return useConstant ? constantVariable.Value : variable.Value;
        }
        set
        {
            variable.Value = useConstant ? constantVariable.Value : value;
        }
    }
}
