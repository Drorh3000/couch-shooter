using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ActivatedFloatVariable : ScriptableObject
{
    public float Value;
    public bool Activated;
}
