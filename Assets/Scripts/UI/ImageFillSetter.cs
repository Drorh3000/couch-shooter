using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFillSetter : MonoBehaviour
{
    public FloatVariable Variable;
    public FloatVariable Max;

    public Image image;

    private void Update()
    {
        image.fillAmount = Mathf.Clamp01(Variable.Value / Max.Value);
    }
}
