using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{

    public AudioMixer audioMixer;

    public Slider volumeSlider;

    public Toggle fullScreenToggle;

    public Dropdown resolutionDropdown;

    List<Resolution> resolutions = new List<Resolution>();

    void Start()
    {
        restoreVolumeSettings();
        restoreFullScreenSettings();
        restoreResolutionSettings();
    }

    private void restoreVolumeSettings()
    {
        float volume;
        audioMixer.GetFloat("masterVolume", out volume);
        volumeSlider.value = Mathf.Pow(10, (volume / 20));
    }

    private void restoreFullScreenSettings()
    {
        fullScreenToggle.isOn = Screen.fullScreen;
    }

    private void restoreResolutionSettings()
    {
        resolutionDropdown.ClearOptions();

        List<string> uniqueResolutions = Screen.resolutions.Aggregate<Resolution, List<string>>(new List<string>(),
         (uniqueValues, nextRes) =>
        {
            string resText = nextRes.width + " x " + nextRes.height;

            if (!uniqueValues.Contains(resText))
            {
                uniqueValues.Add(resText);
                resolutions.Add(nextRes);
            }

            return uniqueValues;
        });

        resolutionDropdown.AddOptions(uniqueResolutions);
        resolutionDropdown.value = resolutions.FindIndex((res) => (res.width == Screen.width) && (res.height == Screen.height));
        resolutionDropdown.RefreshShownValue();
    }

    public void QuitSettings()
    {
        SceneManager.UnloadSceneAsync("SettingsMenu");
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("masterVolume", Mathf.Log10(volume) * 20);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Screen.SetResolution(resolutions[resolutionIndex].width,
         resolutions[resolutionIndex].height,
          Screen.fullScreen);
    }
}
