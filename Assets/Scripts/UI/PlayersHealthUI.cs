using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersHealthUI : MonoBehaviour
{
    private List<Text> hpUI = new List<Text>();

    void Start()
    {
        Text[] hpTextx = GetComponentsInChildren<Text>();
        hpUI.Add(hpTextx[0]);
        hpUI.Add(hpTextx[1]);
        hpUI.Add(hpTextx[2]);
        hpUI.Add(hpTextx[3]);
    }

    private void Update()
    {
        for (int key = 0; key < 4; key++)
        {
            hpUI[key].text = PlayersManager.Instance.players.ContainsKey(key) ?
                                    "" + PlayersManager.Instance.players[key].GetComponent<PlayerHealth>().healthPoints :
                                    "";
        }
    }
}
