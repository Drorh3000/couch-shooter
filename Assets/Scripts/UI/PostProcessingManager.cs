using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessingManager : MonoBehaviour
{
    public Volume volume;

    private PostProcessingManager _instance;

    public static PostProcessingManager Instance { get; }

    private static ChromaticAberration chromaticAberration;

    private static float chromaticAberrationIntensity;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        chromaticAberrationIntensity = 0f;
        volume.profile.TryGet<ChromaticAberration>(out chromaticAberration);
    }

    public static void hardHit(float hitPower)
    {
        if (chromaticAberration)
        {
            chromaticAberration.intensity.Override(hitPower);
            chromaticAberrationIntensity = hitPower;
        }
    }

    private void Update()
    {
        if (chromaticAberration && ((float)chromaticAberration.intensity) > 0f)
        {
            chromaticAberrationIntensity -= 3f * Time.deltaTime;
            chromaticAberration.intensity.Override(chromaticAberrationIntensity);
        }
    }
}
