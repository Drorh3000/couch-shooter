using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSetter : MonoBehaviour
{
    [SerializeField]
    private IntVariable Variable;

    [SerializeField]
    private Text text;

    private void Update()
    {
        text.text = "" + Variable.Value;
    }
}
