using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayInfoAtCursor : MonoBehaviour
{
    [SerializeField]
    ActivatedFloatVariable activatedFloatVariable;

    [SerializeField]
    private Text text;

    void Update()
    {
        if (!activatedFloatVariable.Activated)
        {
            text.text = "";
            return;
        }

        text.text = string.Format("{0:0.00}", activatedFloatVariable.Value);
        //transform.position = Input.mousePosition;
    }
}
