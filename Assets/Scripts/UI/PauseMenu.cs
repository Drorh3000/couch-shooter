using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool isGamePaused = false;

    public GameObject PauseMenuUI;

    public void onPause(InputAction.CallbackContext context)
    {
        if (!context.performed) return;

        if (isGamePaused)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void ResumeGame()
    {
        PauseMenuUI.SetActive(false);
        isGamePaused = false;
    }

    void PauseGame()
    {
        PauseMenuUI.SetActive(true);
        isGamePaused = true;
    }

    public void QuitGame()
    {
        LoadingData.sceneToLoad = "MainMenu";
        SceneManager.LoadScene("LoadingScreen");
        isGamePaused = false;
    }

    public void GoToSettings()
    {
        SceneManager.LoadScene("SettingsMenu", LoadSceneMode.Additive);
    }
}
