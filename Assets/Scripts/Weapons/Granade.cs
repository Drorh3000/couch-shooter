using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granade : MonoBehaviour
{
    [SerializeField]
    private float hitKnockback;

    [SerializeField]
    private float damage = 25f;

    [SerializeField]
    private float explosionRadius;

    [SerializeField]
    private float timer = 5f;

    private void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            explode();
        }
    }

    private void explode()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);

        foreach (Collider2D collider in colliders)
        {
            Rigidbody2D rb;
            if ((rb = collider.GetComponent<Rigidbody2D>()) != null &&
                Physics2D.Raycast(transform.position, rb.transform.position - transform.position, (rb.transform.position - transform.position).magnitude) != false)
            {
                Vector2 vector2 = rb.position - new Vector2(transform.position.x, transform.position.y);
                rb.velocity += vector2.normalized * hitKnockback;

                PlayerHealth player;
                if ((player = rb.GetComponent<PlayerHealth>()) != null)
                {
                    player.takeDamage(damage);
                }
            }
        }

        Destroy(gameObject);
    }
}