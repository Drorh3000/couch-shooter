using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hittable : MonoBehaviour
{
    protected bool isAlive = true;

    [SerializeField]
    protected float healthPoints;

    [SerializeField]
    AnimationCurve curve;

    [SerializeField]
    ActivatedFloatVariable healthPointsUI;

    [SerializeField]
    private GameObject coinPrefab;

    public virtual void takeDamage(float damage, float randomOffset = 5f)
    {
        damage *= Utils.randomValueFromCurve(curve) + 1;

        healthPoints -= damage;
        if (healthPoints <= 0) { noHealth(); }
    }

    protected virtual void noHealth()
    {
        isAlive = false;

        OnMouseExit();
        dropCoins();
        Destroy(gameObject);
    }

    protected virtual void dropCoins(float chanceOfDrop = 0.025f)
    {
        float random = Random.Range(0, 10000) / 10000f;
        if(random <= chanceOfDrop)
        {
            Instantiate(coinPrefab, transform.position, transform.rotation);
        }
    }

    private void OnMouseOver()
    {
        healthPointsUI.Value = healthPoints;
        healthPointsUI.Activated = true;
    }

    private void OnMouseExit()
    {
        healthPointsUI.Activated = false;
    }
}
