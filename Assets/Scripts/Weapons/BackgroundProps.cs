using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundProps : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer[] allSprites = transform.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sr in allSprites)
        {
            sr.sortingOrder = -1;
        }

        Collider2D[] colliders2D = transform.GetComponentsInChildren<Collider2D>();
        foreach (Collider2D collider2D in colliders2D)
        {
            collider2D.enabled = false;
        }
    }
}
