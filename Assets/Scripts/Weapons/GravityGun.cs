using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityGun : ItemImplementation
{
    private new void Start()
    {
        base.Start();
        this.folderLocation += this.sprite + "/";
    }

    override public void mainUse()
    {
        Vector2 aimPosition = transform.root.GetComponentInChildren<PlayerMovement>().aimPosition.normalized;
        Physics2D.gravity = Physics2D.gravity.magnitude * aimPosition;

        foreach (GameObject player in PlayersManager.Instance.players.Values)
        {
            player.GetComponent<PlayerPhysics>().gravityDirection = aimPosition;
        }
    }
    override public void seconderyUse()
    {
        print("KACHING BENGE BENGE BENGE");
    }
}