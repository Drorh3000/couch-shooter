using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;


public class Pickup : MonoBehaviour
{
    [SerializeField]
    private float pickupTimeout;

    public float currPickupTimeout;

    private bool canPickup;

    private void Start()
    {
        currPickupTimeout = pickupTimeout;
    }

    private void Update()
    {
        currPickupTimeout = Mathf.Min(currPickupTimeout + Time.deltaTime, pickupTimeout);
        canPickup = currPickupTimeout == pickupTimeout;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (!canPickup) return;

        PickableItem obj = other.GetComponent<PickableItem>();
        if (obj != null)
        {
            GameObject hand = transform.root.Find("Hands").gameObject;
            if (!hand.GetComponent<UseItem>().isHoldingItem)
            {
                obj.pickUp(hand);
            }
        }
    }
}
