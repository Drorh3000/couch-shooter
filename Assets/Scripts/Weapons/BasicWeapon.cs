using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicWeapon : ItemImplementation
{
    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    private float projectileSpeed;

    [SerializeField]
    private bool changableProjectileSpeed;

    [SerializeField]
    private float knockbackForce;

    private new void Start()
    {
        base.Start();
        this.folderLocation += this.sprite + "/";

        SimpleBullet bullet = bulletPrefab.GetComponent<SimpleBullet>();
        bullet.knockbackForce = knockbackForce;
    }

    override public void mainUse()
    {
        Vector2 aimPosition = transform.root.GetComponentInChildren<PlayerMovement>().aimPosition;

        GameObject bullet = Instantiate(bulletPrefab, gameObject.transform.GetChild(0).GetChild(0).transform.position, new Quaternion());
        bullet.GetComponent<Rigidbody2D>().velocity = ((changableProjectileSpeed ? aimPosition : aimPosition.normalized) * projectileSpeed);
    }
    override public void seconderyUse()
    {
        print("KACHING BENGE BENGE BENGE");
    }
}