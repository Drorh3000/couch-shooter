using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour
{
    public GameObject itemPrefab;

    protected void Start()
    {
        gameObject.layer = LayerMask.NameToLayer("Pickable");
    }

    public void pickUp(GameObject hand)
    {
        Instantiate(itemPrefab, hand.transform.position, new Quaternion(), hand.transform);

        UseItem useItem = hand.GetComponent<UseItem>();
        useItem.isHoldingItem = true;

        Destroy(gameObject);
    }
}
