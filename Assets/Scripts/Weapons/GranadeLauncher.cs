using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeLauncher : ItemImplementation
{
    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    private float projectileSpeed;

    private new void Start()
    {
        base.Start();
        this.sprite = "GranadeLauncher";
        this.folderLocation += this.sprite + "/";
    }

    override public void mainUse()
    {
        Vector2 aimPosition = transform.root.GetComponentInChildren<PlayerMovement>().aimPosition;

        GameObject bullet = Instantiate(bulletPrefab, gameObject.transform.GetChild(0).GetChild(0).transform.position, new Quaternion());
        bullet.GetComponent<Rigidbody2D>().velocity = (aimPosition * projectileSpeed);
    }
    override public void seconderyUse()
    {
        print("KACHING BENGE BENGE BENGE");
    }
}