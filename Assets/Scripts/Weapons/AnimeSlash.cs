using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimeSlash : ItemImplementation
{
    [SerializeField]
    private float damage;

    [SerializeField]
    private float jumpDistance;

    [SerializeField]
    private float velocity;

    private GameObject player;
    private Rigidbody2D playerRigidBody2D;

    private new void Start()
    {
        base.Start();
        this.folderLocation += this.sprite + "/";

        player = transform.root.gameObject;
        playerRigidBody2D = transform.root.GetComponent<Rigidbody2D>();
    }

    override public void mainUse()
    {
        Vector2 aimPosition = transform.root.GetComponentInChildren<PlayerMovement>().aimPosition;
        Vector2 prevPosition = player.transform.position;

        player.transform.position = player.transform.position + (Vector3)(aimPosition * jumpDistance);
        playerRigidBody2D.velocity = (aimPosition * jumpDistance) * velocity;

        RaycastHit2D[] raycastHit2Ds = Physics2D.RaycastAll(prevPosition, (Vector2)player.transform.position - prevPosition, (aimPosition * jumpDistance).magnitude);
        foreach (RaycastHit2D raycastHit2D in raycastHit2Ds)
        {
            if (raycastHit2D.collider.gameObject == this.transform.root.gameObject)
            {
                continue;
            }

            PlayerHealth player;
            if ((player = raycastHit2D.collider.gameObject.GetComponent<PlayerHealth>()) != null)
            {
                player.takeDamage(damage);
            }
        }
    }
    override public void seconderyUse()
    {
        print("KACHING BENGE BENGE BENGE");
    }
}