using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : ItemImplementation
{
    [SerializeField]
    private float hitKnockback;

    [SerializeField]
    private float damage = 25f;
    
    [SerializeField]
    private float swordRange;

    private new void Start()
    {
        base.Start();
        this.sprite = "Sword";
        this.folderLocation += this.sprite + "/";

        this.flipRotation = true;
    }

    override public void mainUse()
    {
        int ignoredLayers = ~LayerMask.GetMask("Player", "Ignore Raycast");

        RaycastHit2D raycastHit2D = Physics2D.Raycast(transform.position,
         (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized,
         swordRange,
         ignoredLayers);

        Rigidbody2D otherRB;

        if (raycastHit2D.collider != null && (otherRB = raycastHit2D.collider.GetComponent<Rigidbody2D>()) != null)
        {
            otherRB.velocity += (otherRB.position - new Vector2(transform.position.x, transform.position.y)).normalized * hitKnockback;

            Hittable hittable;
            if ((hittable = raycastHit2D.collider.gameObject.GetComponent<Hittable>()) != null)
            {
                hittable.takeDamage(damage);
            }
        }
    }
    override public void seconderyUse()
    {
        print("Shhhhhing");
    }
}
