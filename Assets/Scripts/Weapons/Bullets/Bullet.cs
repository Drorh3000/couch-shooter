using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    protected float damage;

    [SerializeField]
    private float timeToLiveInSeconds;

    private float aliveTime;

    private void Awake()
    {
        aliveTime = 0f;
    }
    protected void Update()
    {
        aliveTime += Time.deltaTime;

        if (aliveTime >= timeToLiveInSeconds)
        {
            Destroy(gameObject);
        }
    }
}
