using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyBullet : Bullet
{
    private Rigidbody2D rigidbody2D;

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerHealth playerHealth;
        if ((playerHealth = collision.collider.gameObject.GetComponent<PlayerHealth>()) != null)
        {
            playerHealth.takeDamage(damage);
        }
    }

    private void Update()
    {
        if (rigidbody2D.velocity.magnitude <= 20f)
        {
            Destroy(gameObject);
        }
    }
}
