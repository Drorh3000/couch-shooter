using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : Bullet
{
    public float knockbackForce;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Floor" || collision.tag == "Player")
        {
            PlayerHealth playerHealth;
            if ((playerHealth = collision.gameObject.GetComponent<PlayerHealth>()) != null)
            {
                Rigidbody2D playerRb = collision.gameObject.GetComponent<Rigidbody2D>();

                playerHealth.takeDamage(damage);
                playerRb.AddForce(gameObject.GetComponent<Rigidbody2D>().velocity.normalized * knockbackForce, ForceMode2D.Impulse);
            }

            Destroy(gameObject);
        }
    }
}
