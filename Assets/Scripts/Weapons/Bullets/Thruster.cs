using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : Bullet
{
    private Rigidbody2D rb;

    private Vector2 forceNormalized;
    private Rigidbody2D stickedPlayer;

    [SerializeField]
    private float forceAmount;

    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (stickedPlayer == null && (collision.tag == "Player" || collision.tag == "Floor"))
        {
            forceNormalized = rb.velocity.normalized * forceAmount;
            forceNormalized.y *= 0.5f;

            rb.isKinematic = true;
            rb.simulated = false;
            rb.velocity = Vector2.zero;
            gameObject.transform.parent = collision.transform;

            if (collision.gameObject.tag == "Player")
            {
                stickedPlayer = collision.gameObject.GetComponent<Rigidbody2D>();
            }
        }
    }

    private void Update()
    {
        base.Update();

        if (stickedPlayer != null)
        {
            stickedPlayer.AddForce(forceNormalized, ForceMode2D.Impulse);
        }
    }
}
