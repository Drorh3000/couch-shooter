using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemImplementation : MonoBehaviour
{
    protected string folderLocation = "Weapons/";
    private string spritePrefix = "Pickable";

    [SerializeField]
    protected string sprite = "";

    [SerializeField]
    protected bool flipRotation;

    private float throwDistance = 1.5f;

    private float rotationSpeed = 8f;

    private float throwVel = 5f;

    public bool unlimitedFireRate;

    [SerializeField]
    public float coolDown;

    public float currCoolDown;

    private SpriteRenderer spriteRenderer;
    private Transform spriteObj;

    public void preMainUse()
    {
        if (currCoolDown >= coolDown)
        {
            mainUse();
            currCoolDown = 0f;
        }
    }

    public virtual void mainUse() { }
    public virtual void seconderyUse() { }

    public virtual GameObject throwItem(Vector2 throwDir)
    {
        GameObject pickable = (GameObject)Instantiate(Resources.Load(folderLocation + spritePrefix + sprite),
                                                        transform.position + throwDistance * (Vector3)throwDir,
                                                        transform.rotation);

        SpriteRenderer sr = pickable.transform.GetChild(0).GetComponent<SpriteRenderer>();
        sr.flipX = spriteObj.localScale.x > 0;
        sr.flipY = spriteObj.localScale.y < 0;

        Vector2 playerVel = transform.root.GetComponent<Rigidbody2D>().velocity;
        pickable.GetComponent<Rigidbody2D>().velocity = throwVel * (Vector3)throwDir + (Vector3)playerVel;

        transform.root.GetComponentInChildren<Pickup>().currPickupTimeout = 0f;
        transform.parent.GetComponent<UseItem>().isHoldingItem = false;
        DestroyImmediate(gameObject);

        return pickable;
    }

    protected void Start()
    {
        flipRotation = false;

        currCoolDown = coolDown;

        spriteObj = transform.GetChild(0);

        spriteRenderer = this.transform.GetChild(0).GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = true;
    }

    protected void Update()
    {
        currCoolDown = Mathf.Min(Time.deltaTime + currCoolDown, coolDown);

        Vector2 aimPosition = transform.root.GetComponentInChildren<PlayerMovement>().aimPosition;

        float angle = Mathf.Atan2(aimPosition.y, aimPosition.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);

        if (!flipRotation)
        {
            spriteObj.localScale = new Vector3(spriteObj.localScale.x,
            Mathf.Abs(spriteObj.localScale.y) * (transform.localEulerAngles.z > 90 && transform.localEulerAngles.z < 270 ? -1 : 1),
            spriteObj.localScale.z);
        }
        else
        {
            spriteObj.localScale = new Vector3(
                Mathf.Abs(spriteObj.localScale.x) * (transform.localEulerAngles.z > 90 && transform.localEulerAngles.z < 270 ? -1 : 1),
                        spriteObj.localScale.y,
                        spriteObj.localScale.z);
        }
    }
}
