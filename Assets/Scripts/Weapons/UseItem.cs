using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UseItem : MonoBehaviour
{
    public GameObject itemPrefab;

    private ItemImplementation itemImplementation;

    private bool isMainUsePressed = false;

    [SerializeField]
    private bool _isHoldingItem = false;

    public bool isHoldingItem
    {
        get
        {
            return _isHoldingItem;
        }
        set
        {
            _isHoldingItem = value;
            itemPrefab = value ? gameObject.transform.GetChild(0).gameObject : null;
            if (itemPrefab != null)
            {
                itemPrefab.transform.position = transform.position;
                itemImplementation = itemPrefab.GetComponent<ItemImplementation>();
            }

        }
    }

    private void Update()
    {
        if (isMainUsePressed && isHoldingItem)
        {
            itemImplementation.preMainUse();
        }
    }

    public void onMainUse(InputAction.CallbackContext context)
    {
        if (!gameObject.scene.IsValid() || !isHoldingItem)
        {
            isMainUsePressed = false;
            return;
        }

        if (context.performed)
        {
            isMainUsePressed = true;
        }
        else if (context.canceled)
        {
            isMainUsePressed = false;

            if (itemImplementation.unlimitedFireRate)
            {
                itemImplementation.currCoolDown = itemImplementation.coolDown;
            }
        }
    }
    public void onSeconderyUse(InputAction.CallbackContext context)
    {
        if (!gameObject.scene.IsValid() || !context.performed) return;

        if (isHoldingItem)
        {
            itemImplementation.seconderyUse();
        }
    }
    public void onThrowItem(InputAction.CallbackContext context)
    {
        if (!gameObject.scene.IsValid() || !context.performed) return;

        if (isHoldingItem)
        {
            itemImplementation.throwItem(transform.root.GetComponentInChildren<PlayerMovement>().aimPosition.normalized);
        }
    }
}
