using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayersManager : MonoBehaviour
{
    public static PlayersManager Instance;

    [SerializeField]
    public GameObject spawner;

    public Dictionary<int, GameObject> players;

    public int nextPlayerNum = 0;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        players = new Dictionary<int, GameObject>();
    }
}
