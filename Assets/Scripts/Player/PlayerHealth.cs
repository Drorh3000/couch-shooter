using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float healthPoints;

    public float maxHealthPoints;

    public bool isAlive = true;

    private void Awake()
    {
        healthPoints = maxHealthPoints;
    }

    private void Update()
    {
        checkOutOfBounds();

        if (healthPoints <= 0f)
        {
            isAlive = false;

            throwItem();
        }
    }

    public void takeDamage(float damage)
    {
        healthPoints -= damage;
    }

    private void throwItem()
    {
        ItemImplementation item = gameObject.GetComponentInChildren<ItemImplementation>();
        if (item)
        {
            item.throwItem(gameObject.GetComponent<PlayerMovement>().aimPosition.normalized);
        }
    }

    private void checkOutOfBounds()
    {
        if (transform.position.y <= -15 ||
            transform.position.y >= 15 ||
            transform.position.x >= 25 ||
            transform.position.x <= -25)
        {
            isAlive = false;
        }
    }
}
