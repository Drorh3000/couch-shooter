using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysics : MonoBehaviour
{
    private Rigidbody2D playerRb;

    [Tooltip("Velocity(m/s) being subtracted each second")]
    [SerializeField]
    private float gravity = 40f;

    [SerializeField]
    private float raycastDistance;

    [SerializeField]
    private LayerMask groundLayer;

    [SerializeField]
    private int onWalls = 0;

    private PlayerMovement playerMovement;

    public Vector2 gravityDirection;

    void Start()
    {
        playerRb = this.GetComponent<Rigidbody2D>();
        playerMovement = this.GetComponent<PlayerMovement>();
        groundLayer = LayerMask.GetMask("Ground");

        gravityDirection = new Vector2(0f, -1f);
    }

    public float getGravity()
    {
        return this.gravity;
    }

    public bool getOnWalls()
    {
        return this.onWalls != 0;
    }

    private void FixedUpdate()
    {
        transform.eulerAngles = new Vector3(0f, 0f, gravityDirection.y <= 0 ? 0f : 180f);

        onWalls = calcIsOnWalls() == playerMovement.getDirection() ? (int)playerMovement.getDirection() : 0;
        if (onWalls == 0) calcGravity();
    }

    private void calcGravity()
    {
        playerRb.velocity += gravityDirection * (gravity * Time.fixedDeltaTime);
    }

    private int calcIsOnWalls()
    {
        if (Physics2D.Raycast(playerRb.position, Vector2.left, raycastDistance, groundLayer).collider)
        {
            return -1;
        }
        else if (Physics2D.Raycast(playerRb.position, Vector2.right, raycastDistance, groundLayer).collider)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
