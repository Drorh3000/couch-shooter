using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerFloorCollision : MonoBehaviour
{
    public bool isTriggered = false;

    private void OnTriggerStay2D(Collider2D collision)
    {
        isTriggered = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        isTriggered = false;
    }
}
