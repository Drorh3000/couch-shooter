using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D playerRb;
    private PlayerPhysics playerPy;

    [SerializeField]
    private playerFloorCollision floorColl;

    [Tooltip("The jump height of the player in meters")]
    [SerializeField]
    private float jumpHeight;

    private float jumpVel;

    [SerializeField]
    private float horizontalSpeed = 5f;

    [SerializeField]
    private float accelerationRate, deaccelerationRate, velPower, frictionAmount;

    [SerializeField]
    private float wallSlideVel = 1f;

    private float direction = 0f;

    public Vector2 aimPosition;

    public bool isMouse = false;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        floorColl = this.GetComponentInChildren<playerFloorCollision>();
        playerRb = this.GetComponent<Rigidbody2D>();
        playerPy = this.GetComponent<PlayerPhysics>();

        //Get the velocity needed to get to the given height, using formula: v^2 = u^2 +2as
        jumpVel = Mathf.Sqrt(2 * playerPy.getGravity() * jumpHeight);
    }

    public float getDirection()
    {
        return direction;
    }

    void Update()
    {
        if (isMouse)
        {
            //The / is for the jumper knife, it must be equal to the jumping distance
            Vector2 value = (Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()) - transform.root.position) / 4f;
            aimPosition = value.magnitude < 1f ? value : value.normalized;
        }

        if (playerPy.getOnWalls())
        {
            handleWallSlide();
        }
    }

    private void FixedUpdate()
    {
        float targetSpeed = direction * horizontalSpeed;
        float speedDifference = targetSpeed - playerRb.velocity.x;

        float accelRate = Mathf.Abs(targetSpeed) > 0.001f ? accelerationRate : deaccelerationRate;
        float movement = Mathf.Pow(Mathf.Abs(speedDifference) * accelRate, velPower) * Math.Sign(speedDifference);
        playerRb.AddForce(movement * Vector2.right);

        if (floorColl.isTriggered && direction == 0)
        {
            float amount = Mathf.Min(Mathf.Abs(playerRb.velocity.x), frictionAmount);
            amount *= Mathf.Sign(playerRb.velocity.x);

            playerRb.AddForce(Vector2.right * -amount, ForceMode2D.Impulse);
        }
    }

    private void handleWallSlide()
    {
        playerRb.velocity = new Vector2(playerRb.velocity.x, Mathf.Lerp(playerRb.velocity.y, wallSlideVel, 0.01f));

    }

    public void onJump(InputAction.CallbackContext context)
    {
        if (!gameObject.scene.IsValid() || !context.performed) return;

        if (floorColl.isTriggered)
        {
            playerRb.velocity = new Vector2(playerRb.velocity.x, jumpVel * -(playerPy.gravityDirection.y / Mathf.Abs(playerPy.gravityDirection.y)));
        }
    }

    public void onWalk(InputAction.CallbackContext context)
    {
        if (!gameObject.scene.IsValid() || !context.performed) return;

        direction = context.ReadValue<Vector2>().x;
    }

    public void onAim(InputAction.CallbackContext context)
    {
        if (!gameObject.scene.IsValid() || !context.performed) return;

        if (context.action.activeControl.device.displayName == "Mouse")
        {
            isMouse = true;
        }
        else if (context.ReadValue<Vector2>().magnitude >= 0.15f)
        {
            Vector2 value = context.ReadValue<Vector2>();
            aimPosition = value.magnitude < 1f ? value : value.normalized;
        }
    }
}
