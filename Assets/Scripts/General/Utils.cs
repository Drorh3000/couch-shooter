using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static bool IsNull<T>(this T myObject, string message = "") where T : class
    {
        if (myObject is UnityEngine.Object obj)
        {
            if (!obj)
            {
                Debug.LogError("The object is null! " + message);
                return false;
            }
        }
        else
        {
            if (myObject == null)
            {
                Debug.LogError("The object is null! " + message);
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Gets a curve and returns a y value on it
    /// </summary>
    /// <returns>A random value between 0 - 1 with given curve as probability.</returns>
    public static float randomValueFromCurve(AnimationCurve curve)
    {
        return curve.Evaluate(Random.Range(0f, 1f));
    }
}
