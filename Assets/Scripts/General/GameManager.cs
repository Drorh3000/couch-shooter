using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    private static GameManager Instance;

    private bool singlePlayerAlive = false;

    private string[] arrayOfScenesName;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        int sceneNumber = SceneManager.sceneCountInBuildSettings;

        arrayOfScenesName = new string[sceneNumber];
        for (int i = 0; i < sceneNumber; i++)
        {
            arrayOfScenesName[i] = Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
        }

        Physics2D.IgnoreLayerCollision(8, 8);
    }

    public void OnPlayerJoined(PlayerInput playerInput)
    {
        while (PlayersManager.Instance.players.ContainsKey(PlayersManager.Instance.nextPlayerNum))
        {
            PlayersManager.Instance.nextPlayerNum++;
        }
        PlayersManager.Instance.players.Add(PlayersManager.Instance.nextPlayerNum, playerInput.gameObject);

        SetPlayerInfo(playerInput.transform);
    }

    public void OnPlayerLeft(PlayerInput playerInput)
    {
        foreach (int playerNum in PlayersManager.Instance.players.Keys)
        {
            if (PlayersManager.Instance.players[playerNum].Equals(playerInput.gameObject))
            {
                PlayersManager.Instance.players.Remove(playerNum);
                PlayersManager.Instance.nextPlayerNum = playerNum;

                break;
            }
        }

        resetPlayer(playerInput.gameObject, false);
        Destroy(playerInput.gameObject);
    }

    void SetPlayerInfo(Transform player)
    {
        player.transform.SetPositionAndRotation(PlayersManager.Instance.spawner.transform.position, PlayersManager.Instance.spawner.transform.rotation);
        player.GetComponentInChildren<SpriteRenderer>().color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        player.gameObject.name = "Player" + PlayersManager.Instance.nextPlayerNum;
    }

    private void Update()
    {
        IEnumerable<GameObject> alivePlayers = PlayersManager.Instance.players.Values.Where((player) => player.GetComponent<PlayerHealth>().isAlive);
        int alivePlayersCount = 0;

        foreach (GameObject gameObj in alivePlayers)
        {
            alivePlayersCount++;
        }

        singlePlayerAlive = PlayersManager.Instance.players.Count >= 2 && alivePlayersCount <= 1;
        if (singlePlayerAlive)
        {
            int currentScene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(arrayOfScenesName[(currentScene + 1) % arrayOfScenesName.Count()]);

            foreach (GameObject player in PlayersManager.Instance.players.Values)
            {
                resetPlayer(player);
                resetLevel();
            }
        }
    }

    private void resetLevel()
    {
        Vector2 defaultGravityDirection = new Vector2(0, -1f);

        Physics2D.gravity = Physics2D.gravity.magnitude * defaultGravityDirection;

        foreach (GameObject player in PlayersManager.Instance.players.Values)
        {
            player.GetComponent<PlayerPhysics>().gravityDirection = defaultGravityDirection;
        }
    }

    private void resetPlayer(GameObject player, bool destroyHeldItem = true)
    {
        ItemImplementation heldItem;
        if ((heldItem = player.GetComponentInChildren<ItemImplementation>()) != null)
        {
            GameObject pickable = heldItem.throwItem(Vector2.up);

            if (pickable && destroyHeldItem) Destroy(pickable);
        }

        player.transform.SetPositionAndRotation(PlayersManager.Instance.spawner.transform.position, PlayersManager.Instance.spawner.transform.rotation);
        player.GetComponent<PlayerHealth>().healthPoints = player.GetComponent<PlayerHealth>().maxHealthPoints;
        player.GetComponent<PlayerHealth>().isAlive = true;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}
